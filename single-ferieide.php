<?php get_template_part('parts/header'); the_post(); ?>

<main>

  <?php get_template_part('parts/layouts/layout', 'slider');  ?>

  <section class="single">
    <div class="wrap hpad">

      <a class="single__btn btn--back" onclick="window.history.go(-1); return false;"><?php _e('Tilbage', 'lionlab'); ?></a>

      <div class="row">

        <article class="single__content col-sm-12" itemscope itemtype="http://schema.org/TouristAttraction">

          <header>
            <h1 class="single__title h2" itemprop="name">
              <?php the_title(); ?>
            </h1>
          </header>

          <div itemprop="description">
            <?php the_content(); ?>
          </div>
            
        </article>

        <?php 
          //ACF variables
          $adress = get_field('address');
          $mail = get_field('email');
          $url = get_field('website');
          $location = get_field('location');

          // Remove http:// or https:// from website field
          $url = preg_replace( "#^[^:/.]*[:/]+#i", "", $url );
        ?> 
          
        <div class="col-sm-12">
          <div class="row flex flex--wrap">

            <div class="col-lg-4 col-sm-6 single__info-column flex flex--wrap">
              <?php if ($url) : ?>
                <div class="single__info">
                  <h6 class="single__info-title"><?php _e('Website', 'lionlab'); ?></h6>
                  <a class="single__info-customer-link" target="_blank" href="<?php echo esc_url($url); ?>"><?php echo esc_html($url); ?></a>
                </div>
              <?php endif; ?>       

              <?php if ($mail) : ?>
                <div class="single__info">
                  <h6 class="single__info-title"><?php _e('Kontakt', 'lionlab'); ?></h6>
                  <a class="single__info-mail-link" href="mailto:<?php echo esc_html($mail); ?>"><?php echo esc_html($mail); ?></a>
                </div>
              <?php endif; ?>

              <?php if ($adress) : ?>
                <div class="single__info">
                  <h6 class="single__info-title"><?php _e('Adresse', 'lionlab'); ?></h6>
                  <p itemprop="address"><?php echo $adress; ?></p>
                </div>
              <?php endif; ?>
             </div>
             
             <div class="col-lg-4 col-sm-6 single__info-column">
              <h6 class="single__info-title"><?php _e('Map', 'lionlab'); ?></h6>
              <?php get_template_part('parts/google-map', 'single'); ?>
             </div>

            <?php get_template_part('parts/gallery'); ?>

          </div>
        </div>

      </div>

    </div>
  </section>

  <?php 
    //get product ID's array
    $product_links = get_field('product_link');

    // Custom WP query query
    $args = array(
      'post_type' => 'product',
      'stock' => 1,
      'posts_per_page' => -1,
      'post__in' => $product_links,
      'orderby'        => 'post__in'
    );

    $products = new WP_Query($args);


    if ( $products->have_posts() && $product_links ) : ?>

    <section class="single-product woocommerce padding--bottom">
      <div class="wrap hpad">

        <h2><?php _e('Brochure', 'lionlab'); ?></h2>

        <div class="row">
          <?php while ( $products->have_posts() ) : $products->the_post(); global $product; ?>

              <div class="single-product__item col-sm-4">

                <div class="single-product__thumbnail">
                  <?php 
                    if (has_post_thumbnail( $products->post->ID )) echo get_the_post_thumbnail($products->post->ID, 'medium'); else echo '<img src="'.woocommerce_placeholder_img_src().'" alt="My Image Placeholder" width="65px" height="115px" />'; 
                  ?>
                </div>

                <h4 class="single-product__title"><?php the_title(); ?></h4>

                <?php woocommerce_template_single_excerpt($products->post, $product); ?>
                
                <?php 
                  //get product attribute catalog size
                  $size = $product->get_attribute( 'pa_brochure-format' ); 
                ?>
                
                <?php if ($size) : ?>
                  <h6 class="single-product__format"><?php _e('Brochure format', 'lionlab');  ?></h6>
                  <p><em><?php echo esc_html($size); ?></em></p> 
                <?php endif; ?>
  
                <?php woocommerce_template_loop_add_to_cart($products->post, $product); ?> <?php if (get_field('pdf_file') ) : ?>

                <a class="button" target="_blank" href="<?php echo the_field('pdf_file'); ?>"><?php _e('download', 'lionlab'); ?></a>
                <?php endif; ?>

              </div>

            <?php endwhile; wp_reset_postdata(); ?>

        </div>
      </div>
       
    </section>
  <?php endif; ?>

</main>

<?php get_template_part('parts/footer'); ?>