<?php get_template_part('parts/header'); the_post(); ?>

<main>
  <?php get_template_part('parts/slider');  ?>

  <section class="single">
    <div class="wrap hpad">
      <div class="row">

        <article class="col-sm-12" itemscope itemtype="http://schema.org/Product">

          <header>
            <h1 class="h2" itemprop="name">
              <?php the_title(); ?>
            </h1>
          </header>

          <div itemprop="description">
            <?php the_content(); ?>
          </div>

        </article>

      </div>
    </div>
  </section>

</main>

<?php get_template_part('parts/footer'); ?>