<?php get_template_part('parts/header'); ?>

<main>

<?php 
  get_template_part('parts/slider'); 
  get_template_part('parts/page', 'header');


  //get current parent category id
  $parentid = get_queried_object_id();
           
  $args = array(
      'parent' => $parentid
  );
   
  $terms = get_terms( 'category', $args );

  //counter
  $i = 0;

  if (have_posts() && ($parentid) ) : 
?>

<section class="archive__header">
  <div class="wrap hpad">
    <?php echo category_description(); ?>
    <?php get_template_part('parts/category-submenu'); ?>
    <?php get_template_part('parts/search-bar'); ?>
  </div>
</section>

<?php foreach ( $terms as $term ) : 

    //query arguments
    $args = array(
      'posts_per_page' => 16,
      'post_type' => 'ferieide',
      'orderby' => 'rand',
      'category_name' => $term->slug
    );

    //the query
    $query = new WP_Query( $args );

?>

<section class="archive">
    <div class="wrap hpad">

      <h2 class="archive__title"><?php echo $term->name; ?></h2>

      <div class="archive__row flex flex--wrap">
  
        <?php 
          while ($query->have_posts() ): $query->the_post(); 
          
          //post img
          $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'url' );
          //post img alt tag
          $alt = get_post_meta($thumb, '_wp_attachment_image_alt', true);  

          $url = get_field('website'); 

          // Remove http:// or https:// from website field
          $url = preg_replace( "#^[^:/.]*[:/]+#i", "", $url );

          $i++;
          
        ?>

          <button title="<?php the_title_attribute(); ?>" itemscope itemtype="http://schema.org/TouristAttraction" id="attractions--<?php the_title_attribute(); ?>" data-target="attractions--<?php the_title_attribute(); ?>" class="col-sm-3 attractions__item" style="background-image: url(<?php echo esc_url($thumb[0]); ?>);">

            <div class="attractions__content" id="attractions--<?php the_title_attribute(); ?>" data-target="attractions--<?php the_title_attribute(); ?>">
              <header>
              <h2 class="attractions__title h4" itemprop="name">                                
                <?php the_title(); ?>
              </h2>
              </header>

              <div class="attractions__excerpt" itemprop="description">
                <?php echo wp_trim_words(get_the_excerpt(), 20, '...' ); ?>
              </div>
 
              <div class="attractions__wrap flex flex--justify flex--center flex--wrap">
                <a class="attractions__permalink" href="<?php echo the_permalink(); ?>"></a> 
                <a class="attractions__sitelink" target="_blank" href="<?php echo esc_url($url); ?>"><?php echo esc_html($url); ?></a> <span class="btn btn--large">+</span>
              </div>
            </div>
            
            <header class="bg--sand attractions__label">
              <h2 class="attractions__title h4" itemprop="name">                                
                <?php the_title(); ?>
              </h2>
            </header>

          </button>
    
          <?php endwhile; wp_reset_postdata(); ?>
          
      </div>
    </div>
  </section>
<?php endforeach; ?>
<?php endif; ?>


<?php  

  //get top level category to check if is ancestor of it
  $categories = get_the_category();
  $category_id = $categories[0]->cat_ID; 
  $category_parent = $categories->parent; 

  //subcategory attractions
?>
     
<?php if (have_posts() && $category_id && (is_subcategory() ) ) :  ?>

<section class="archive">
    <div class="wrap hpad">

      <div class="archive__row flex flex--wrap">
  
        <?php 
          while (have_posts() ): the_post(); 

          $url = get_field('website'); 

          // Remove http:// or https:// from website field
          $url = preg_replace( "#^[^:/.]*[:/]+#i", "", $url );

          //post img
          $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'url' );
          //post img alt tag
          $alt = get_post_meta($thumb, '_wp_attachment_image_alt', true);   

          $i++;
          
        ?>

          <button title="<?php the_title_attribute(); ?>" itemscope itemtype="http://schema.org/TouristAttraction" id="attractions--<?php the_title_attribute(); ?>" data-target="attractions--<?php the_title_attribute(); ?>" class="col-sm-3 attractions__item attractions__item--shuffle" style="background-image: url(<?php echo esc_url($thumb[0]); ?>);">

            <div class="attractions__content" id="attractions--<?php the_title_attribute(); ?>" data-target="attractions--<?php the_title_attribute(); ?>">
              <header>
              <h2 class="attractions__title h4" itemprop="name">                                
                <?php the_title(); ?>
              </h2>
              </header>

              <div class="attractions__excerpt" itemprop="description">
                <?php echo wp_trim_words(get_the_excerpt(), 30, '...' ); ?>
              </div>

              <div class="attractions__wrap flex flex--justify flex--center">
                <a class="attractions__permalink" href="<?php echo the_permalink(); ?>"></a> 
                <a class="attractions__sitelink" target="_blank" href="<?php echo esc_url($url); ?>"><?php echo esc_html($url); ?></a> <span class="btn btn--large">+</span>
              </div>
            </div>

            <header class="bg--sand attractions__label">
              <h2 class="attractions__title h4" itemprop="name">                                
                <?php the_title(); ?>
              </h2>
            </header>
            
          </button>
    
          <?php endwhile; ?>
          
      </div>
    </div>
  </section>

<?php endif; ?>


</main>

<?php get_template_part('parts/footer'); ?>