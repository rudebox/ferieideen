jQuery(document).ready(function($) {

  //menu toggle
  $('.nav-toggle').click(function(e) {
    e.preventDefault();
    $('.nav--mobile').toggleClass('is-open');
    $('body').toggleClass('is-fixed');
  });


  //attractions toggle - match up input values with search results categories
  var $toggle = $('.attractions__item');

  $toggle.on('click', function(e) {
    var data = $(this).data('target');
    var $content = $('.attractions__item--full');
    var $links = $('.attractions__content');


    if ($('body').hasClass('is-iphone') || ($(window).width() < 769) ) {
      $links.each(function() {
        if ($(this).attr('id') === data ) {
          $(this).slideToggle();
          $('.attractions__permalink, .attractions__sitelink').addClass('is-visible');
        } else {
          $(this).slideUp(); 
        }
      });
    }

    $content.each(function() {
      if ($(this).attr('id') === data ) {
        $(this).slideToggle();
        $(this).removeClass('is-hidden');
      } else {
        $(this).slideUp(); 
        $(this).addClass('is-hidden');
      }
    });

    $toggle.each(function() {

      if ($(this).attr('id') === data ) {
        $(this).not('search-result__item').toggleClass('is-active'); 

      } else {
        $(this).removeClass('is-active');
      } 
    });
  });


  //toggle search categories - match up input values with search results categories
  var $input = $('.search-result__checkbox');

  if ($('.search-result__checkbox--all').prop('checked') === true) {
      $('.search-result__item').addClass('is-visible');
  } 

  $input.on('change', function() {
    var data = $(this).val();
    var $item = $('.search-result__item');

    $item.each(function() {
      if ($(this).attr('data-category') === data) { 
        $(this).addClass('is-visible');
      } else {
        $(this).removeClass('is-visible');  
      }

      if ($('.search-result__checkbox--all').prop('checked') === true) {
        $('.search-result__item').addClass('is-visible');
      } 
    });
  });


  //owl slider/carousel
  var owl = $('.slider__track');

  owl.each(function() {
  $(this).children().length > 1;

    owl.owlCarousel({
        loop: true,
        items: 1,
        autoplay: true,
        // nav: true,
        dots: true,
        autplaySpeed: 11000,
        autoplayTimeout: 10000,
        smartSpeed: 250,
        smartSpeed: 2200,
        navSpeed: 2200
        // navText : ["<i class='fa fa-arrow-left' aria-hidden='true'></i>", "<i class='fa fa-arrow-right' aria-hidden='true'></i>"]
    });
  });


  //owl slider/carousel
  var owl = $('.slider__track--front');

  owl.each(function() {
  $(this).children().length > 1;

    owl.owlCarousel({
        loop: false,
        items: 1,
        autoplay: true,
        // nav: true,
        mouseDrag: false,
        dots: true,
        autplaySpeed: 11000,
        autoplayTimeout: 10000,
        smartSpeed: 250,
        smartSpeed: 2200,
        navSpeed: 2200
        // navText : ["<i class='fa fa-arrow-left' aria-hidden='true'></i>", "<i class='fa fa-arrow-right' aria-hidden='true'></i>"]
    });
  });

  //shuffle
  $posts = $('.attractions__item--shuffle');

  for(var i = 0; i < $posts.length; i++){
      var target = Math.floor(Math.random() * $posts.length -1) + 1;
      var target2 = Math.floor(Math.random() * $posts.length -1) +1;
      $posts.eq(target).before($posts.eq(target2));
  }


  //mobile hover solution

  if($('.attractions__item').hasClass('is-active') ) {
    $('.attractions__content').addClass('is-visible');
  } else {
    $('.attractions__content').removeClass('is-visible');
  }

});