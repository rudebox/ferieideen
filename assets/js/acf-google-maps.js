/**
* Google Maps with ACF
* http://www.advancedcustomfields.com/resources/google-map/
* ================================================== */

(function($) {


    // Settings from data-attributes
    var settings = $('.js-maps').data();


    /**
     * new_map
     * This function will render a Google Map onto the selected jQuery element
     * @param   $el (jQuery element)
     * @return  n/a
     **/

    function new_map( $el ) {
        
        //marker selector
        var $markers = $el.find('.marker');
        
        //center map on resize
        google.maps.event.addDomListener(window, 'resize', function() {
            var center = map.getCenter();
            google.maps.event.trigger(map, 'resize'); 
            map.setCenter(center);
        });


        //configuration
        var args = {
            zoom        : settings.zoom,
            center      : new google.maps.LatLng(0, 0),
            mapTypeId   : google.maps.MapTypeId.ROADMAP,
            scrollwheel : settings.scrollwheel || false,
            draggable   : settings.draggable || false,
            disableDoubleClickZoom: settings.disabledoubleclickzoom || false,
            styles      : settings.styles || [    {
                            "featureType": "transit",
                            "stylers": [ 
                              { "visibility": "off" }
                            ]
                          },{
                            "featureType": "poi",
                            "stylers": [
                              { "visibility": "off" }
                            ]
                          },{
                            "featureType": "landscape",
                            "stylers": [
                              { "visibility": "off" }
                            ]
                          }],
            clickableIcons: false,
        };



        
        
        //create new map     
        var map = new google.maps.Map( $el[0], args);
        
        //add reference to markers
        map.markers = [];
        
        
        //add markers
        $markers.each(function(){
            
         add_marker( $(this), map );
            
        });
        
        //center newly created map
        center_map( map );

        var options = {
          imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m'
        };

        // add marker cluster
        markerCluster( map.markers, map, options, { ignoreHidden: true } );

        
        //return map
        return map;


    }



    /**
     * add_marker
     * This function will add a marker to the selected Google Map
     * @param   $marker (jQuery element)
     * @param   map (Google Map object)
     * @return n/a
     **/

    function add_marker( $marker, map ) {

        //position
        var latlng = new google.maps.LatLng( $marker.attr('data-lat'), $marker.attr('data-lng') );

        //category
        var category = $marker.attr('data-category');

        //custom marker
        var markerIcon = settings.marker;

        if ( typeof markerIcon !== 'undefined' ) {
          var icon = markerIcon
        } else {
          var icon = {
            path        : google.maps.SymbolPath.CIRCLE,
            fillOpacity   : settings.fillopacity,
            fillColor   : settings.colorInner,
            strokeColor   : settings.color,
            strokeWeight  : settings.strokeweight,
            scale       : settings.size,
          }
        }

        console.log(category);


        // Create marker
        var marker = new google.maps.Marker({
            position    : latlng,
            map         : map,
            icon        : icon,
            category    : category
        });

        //add marker to array
        map.markers.push( marker );

        //if marker contains HTML, add it to an infoWindow
        if ( $marker.html() ) {
            // Create info window
            var infowindow = new google.maps.InfoWindow({
                content     : $marker.html()
            });

            //show info window when marker is clicked
            google.maps.event.addListener(marker, 'click', function() {

                infowindow.open( map, marker );

            });
        }
    }

    
    function markerCluster( markers, map, options ) {
      if ($('body').is('.search') ) {
        markerCluster = new MarkerClusterer(map, markers, options, { ignoreHidden: true });
      }
    }



    //filter through markers based on categories 

    filterMarker = function (id) {

      for (i = 0; i < map.markers.length; i++) {
        marker = map.markers[i];

        if (document.getElementById('all').checked) {                          
            marker.setVisible(true);
            markerCluster.setIgnoreHidden(true);
            markerCluster.repaint(marker);
        } else {
            marker.setVisible(false);
            markerCluster.setIgnoreHidden(true);
            markerCluster.repaint(marker);
        } 

        if (document.getElementById(id).checked) {
          if (marker.category == id) {
            marker.setVisible(true);
            markerCluster.setIgnoreHidden(true);
            markerCluster.repaint(marker);
          } 
        } 

        else {
          if (marker.category == id) {
            marker.setVisible(false);
            markerCluster.setIgnoreHidden(true); 
            markerCluster.repaint(marker);
          }
        }
      } 
    }


    /**
     * center_map
     * This function will center the map, showing all markers attached to this map
     * @param   map (Google Map object)
     * @return n/a
     **/

    function center_map( map ) {

        //vars
        var bounds = new google.maps.LatLngBounds();

        //loop through all markers and create bounds
        $.each( map.markers, function( i, marker ){

            var latlng = new google.maps.LatLng( marker.position.lat(), marker.position.lng() );

            bounds.extend( latlng );

        });

        // only 1 marker?
        if ( map.markers.length == 1 )  {
            // set center of map
         map.setCenter( bounds.getCenter() );
         map.setZoom( settings.zoom );

        }   else    {
            // fit to bounds
            map.fitBounds( bounds );
        }

    }

    

    function initAutocomplete( map ) {

      var map = map;

      var input = document.getElementById('pac-input');
      var searchBox = new google.maps.places.SearchBox(input); 


      //bias the SearchBox results towards current map's viewport.
      map.addListener('bounds_changed', function() {
        searchBox.setBounds(map.getBounds());
      });

      searchBox.addListener('places_changed', function () {
        var places = searchBox.getPlaces();

        if (places.length == 0) {
         return;
        }

        var bounds = new google.maps.LatLngBounds();
        places.forEach(function (place) {

          if (place.geometry.viewport) {
            //only geocodes have viewport.
            bounds.union(place.geometry.viewport);
          } else {
            bounds.extend(place.geometry.location);
          }
        });

        map.fitBounds(bounds);

      });
    
    }



    /**
     * document ready
     * This function will render each map when the document is ready (page has loaded)
     * @param   n/a
     * @return  n/a
     **/
    
    //global map variable
    var map = null;

    $(document).ready(function(){

        $('.js-maps').each(function(){
            map = new_map( $(this) );

            // if ($('body').is('.search') ) {
            //   initAutocomplete( map );
            // }
        });

    });

})(jQuery);