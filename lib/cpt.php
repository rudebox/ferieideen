<?php 
// Register Custom Post Type ferieide
function create_ferieide_cpt() {

	$labels = array(
		'name' => _x( 'ferieideer', 'Post Type General Name', 'lionlab' ),
		'singular_name' => _x( 'ferieide', 'Post Type Singular Name', 'lionlab' ),
		'menu_name' => _x( 'Ferieideer', 'Admin Menu text', 'lionlab' ),
		'name_admin_bar' => _x( 'ferieide', 'Add New on Toolbar', 'lionlab' ),
		'archives' => __( 'ferieide Archives', 'lionlab' ),
		'attributes' => __( 'ferieide Attributes', 'lionlab' ),
		'parent_item_colon' => __( 'Parent ferieide:', 'lionlab' ),
		'all_items' => __( 'Alle ferieideer', 'lionlab' ),
		'add_new_item' => __( 'Tilføj ny ferieide', 'lionlab' ),
		'add_new' => __( 'Tilføj ny', 'lionlab' ),
		'new_item' => __( 'Ny ferieide', 'lionlab' ),
		'edit_item' => __( 'Edit ferieide', 'lionlab' ),
		'update_item' => __( 'Update ferieide', 'lionlab' ),
		'view_item' => __( 'View ferieide', 'lionlab' ),
		'view_items' => __( 'View ferieideer', 'lionlab' ),
		'search_items' => __( 'Search ferieide', 'lionlab' ),
		'not_found' => __( 'Not found', 'lionlab' ),
		'not_found_in_trash' => __( 'Not found in Trash', 'lionlab' ),
		'featured_image' => __( 'Featured Image', 'lionlab' ),
		'set_featured_image' => __( 'Set featured image', 'lionlab' ),
		'remove_featured_image' => __( 'Remove featured image', 'lionlab' ),
		'use_featured_image' => __( 'Use as featured image', 'lionlab' ),
		'insert_into_item' => __( 'Insert into ferieide', 'lionlab' ),
		'uploaded_to_this_item' => __( 'Uploaded to this ferieide', 'lionlab' ),
		'items_list' => __( 'ferieideer list', 'lionlab' ),
		'items_list_navigation' => __( 'ferieideer list navigation', 'lionlab' ),
		'filter_items_list' => __( 'Filter ferieideer list', 'lionlab' ),
	);
	$args = array(
		'label' => __( 'ferieide', 'lionlab' ),
		'description' => __( '', 'lionlab' ),
		'labels' => $labels,
		'menu_icon' => 'dashicons-welcome-add-page',
		'supports' => array('title', 'editor', 'excerpt', 'thumbnail', 'post-formats', 'revisions', 'author', 'trackbacks', 'page-attributes'),
		'taxonomies' => array('category'),
		'public' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'menu_position' => 5,
		'show_in_admin_bar' => true,
		'show_in_nav_menus' => true,
		'can_export' => true,
		'has_archive' => true,
		'hierarchical' => false,
		'exclude_from_search' => false,
		'show_in_rest' => true,
		'publicly_queryable' => true,
		'capability_type' => 'post',
	);
	register_post_type( 'ferieide', $args );

}

add_action( 'init', 'create_ferieide_cpt', 0 );

?>