<?php
 /**
   * Description: Lionlab helpers
   *
   * @package Lionlab
   * @subpackage Lionlab
   * @since Version 1.0
   * @author Kaspar Rudbech
   */

	//remove countrycode from phone
	function get_formatted_phone($str) {
	  
	  // Remove +45
	  $str = str_replace('+', '00', $str);

	  // Only allow integers
	  $str = preg_replace('/[^0-9]/s', '', $str);

	  return $str;
	}


	//allow svg uploads
	function cc_mime_types($mimes) {
	  $mimes['svg'] = 'image/svg+xml';
	  return $mimes;
	}
	add_filter('upload_mimes', 'cc_mime_types');


	// Custom excerpt length
	function custom_excerpt_length( $length ) {
	  return 90;
	}

	add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );


	// Custom excerpt text
	function custom_excerpt_more( $more ) {
	  return '&hellip;';
	}

	add_filter('excerpt_more', 'custom_excerpt_more');


	// Move Yoast to bottom
	function yoasttobottom() {
	  return 'low';
	}
	add_filter( 'wpseo_metabox_prio', 'yoasttobottom');


	//get proper title
	function get_proper_title( $id = null  ) {
	  
	  if ( is_null($id) ) {
	    global $post;
	    $id = $post->ID;
	  }

	  $acf_title = get_field('page_title', $id);

	  return ($acf_title) ? $acf_title : get_the_title( $id );
	}


	//force categories of custom post type use default archive too
	function lionlab_show_cpt_archives( $query ) {

	 if( is_category() || is_tag() && empty( $query->query_vars['suppress_filters'] ) ) {
	   $query->set( 'post_type', array(
	   'post', 'nav_menu_item', 'ferieide'
	 ));

	 return $query;
	 }

	}

	add_filter('pre_get_posts', 'lionlab_show_cpt_archives');

	//get primary category
	function get_post_primary_category($post_id, $term='category', $return_all_categories=false){
	    $return = array();

	    if (class_exists('WPSEO_Primary_Term')){
	        // Show Primary category by Yoast if it is enabled & set
	        $wpseo_primary_term = new WPSEO_Primary_Term( $term, $post_id );
	        $primary_term = get_term($wpseo_primary_term->get_primary_term());

	        if (!is_wp_error($primary_term)){
	            $return['primary_category'] = $primary_term;
	        }
	    }

	    if (empty($return['primary_category']) || $return_all_categories){
	        $categories_list = get_the_terms($post_id, $term);

	        if (empty($return['primary_category']) && !empty($categories_list)){
	            $return['primary_category'] = $categories_list[0];  //get the first category
	        }
	        if ($return_all_categories){
	            $return['all_categories'] = array();

	            if (!empty($categories_list)){
	                foreach($categories_list as &$category){
	                    $return['all_categories'][] = $category->term_id;
	                }
	            }
	        }
	    }

	    return $return;
	}



	function is_subcategory () {
	    $cat = get_query_var('cat');
	    $category = get_category($cat);
		$category->parent;
	    return ( $category->parent == '0' ) ? false : true;
	}



	/**
	 * Extend WordPress search to include custom fields
	 *
	 * https://adambalee.com
	 */

	/**
	 * Join posts and postmeta tables
	 *
	 * http://codex.wordpress.org/Plugin_API/Filter_Reference/posts_join
	 */
	function cf_search_join( $join ) {
	    global $wpdb;

	    if ( is_search() ) {    
	        $join .=' LEFT JOIN '.$wpdb->postmeta. ' ON '. $wpdb->posts . '.ID = ' . $wpdb->postmeta . '.post_id ';
	    }

	    return $join;
	}
	add_filter('posts_join', 'cf_search_join' );

	/**
	 * Modify the search query with posts_where
	 *
	 * http://codex.wordpress.org/Plugin_API/Filter_Reference/posts_where
	 */
	function cf_search_where( $where ) {
	    global $pagenow, $wpdb;

	    if ( is_search() ) {
	        $where = preg_replace(
	            "/\(\s*".$wpdb->posts.".post_title\s+LIKE\s*(\'[^\']+\')\s*\)/",
	            "(".$wpdb->posts.".post_title LIKE $1) OR (".$wpdb->postmeta.".meta_value LIKE $1)", $where );
	    }

	    return $where;
	}
	add_filter( 'posts_where', 'cf_search_where' );

	/**
	 * Prevent duplicates
	 *
	 * http://codex.wordpress.org/Plugin_API/Filter_Reference/posts_distinct
	 */
	function cf_search_distinct( $where ) {
	    global $wpdb;

	    if ( is_search() ) {
	        return "DISTINCT";
	    }

	    return $where;
	}

	add_filter( 'posts_distinct', 'cf_search_distinct' );
		
?>