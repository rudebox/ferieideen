<?php 
	//enqueue scripts and styles on woo templates only
	add_action( 'wp_enqueue_scripts', 'lionlab_disable_woocommerce_loading_css_js' );
	 
	function lionlab_disable_woocommerce_loading_css_js() {
	 
		// Check if WooCommerce plugin is active
		if( function_exists( 'is_woocommerce' ) ){
	 
			// Check if it's any of WooCommerce page
			if(! is_woocommerce() && ! is_cart() && ! is_checkout() && ! is_singular('ferieide') ) { 		
				
				//Dequeue WooCommerce styles
				wp_dequeue_style('woocommerce-layout'); 
				wp_dequeue_style('woocommerce-general'); 
				wp_dequeue_style('woocommerce-smallscreen'); 	
	 
				//Dequeue WooCommerce scripts
				wp_dequeue_script('wc-cart-fragments');
				wp_dequeue_script('woocommerce'); 
				wp_dequeue_script('wc-add-to-cart'); 
			
				wp_deregister_script( 'js-cookie' );
				wp_dequeue_script( 'js-cookie' );

				wp_dequeue_script('mailchimp-woocommerce');
			}
		}	
	}

	//disable unnecessary woocommerce features
	add_action( 'template_redirect', function() {
	    remove_theme_support( 'wc-product-gallery-zoom' );
	    remove_theme_support( 'wc-product-gallery-lightbox' );
	    remove_theme_support( 'wc-product-gallery-slider' );
	}, 100 );


	//remove related products output
	remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20 );

	//remove single product page
	add_filter( 'woocommerce_register_post_type_product','hide_product_page',12,1);
	function hide_product_page($args){
	    $args["publicly_queryable"]=false;
	    $args["public"]=false;
	    return $args;
	}


	//custom add to cart button text
	add_filter( 'woocommerce_product_add_to_cart_text', 'woocommerce_custom_product_add_to_cart_text' );  
	function woocommerce_custom_product_add_to_cart_text() {
	    return __( 'Send brochure', 'lionlab' );
	}


	//stop products from indexing
	add_filter('the_seo_framework_robots_meta_array’, ‘my_robots_adjustments', 10, 1 );
		function my_robots_adjustments( $robots = array() ) {

		if ( ! function_exists('is_product') )
			return $robots;

		if ( is_product() ) {
			//keys match the value. Legacy code.
			$robots['noindex'] = 'noindex';
			$robots['nofollow'] = 'nofollow';
		}

		return $robots;
	}


	//clear cart after payment  
	add_action( 'woocommerce_thankyou', 'order_received_empty_cart_action', 10, 1 );
	
	function order_received_empty_cart_action( $order_id ){
	    WC()->cart->empty_cart();
	}

	//add product description to shop page
	add_action( 'woocommerce_after_shop_loop_item_title', 'wc_add_short_description' );
	/**
	 * WooCommerce, Add Short Description to Products on Shop Page
	 *
	 * @link https://wpbeaches.com/woocommerce-add-short-or-long-description-to-products-on-shop-page
	 */
	function wc_add_short_description() {
		global $product;

		?>
	        <div itemprop="description">
	            <?php echo apply_filters( 'woocommerce_short_description', $product->post-> post_excerpt ) ?>
	        </div>
		<?php
	}

	//add brochure size product attribute to shop page
	add_action('woocommerce_after_shop_loop_item_title', 'cstm_display_product_category', 10);

	function cstm_display_product_category() {

		global $product;
		$size = $product->get_attribute('pa_brochure-format');

		if ($size) {
			echo '<h6 class="single-product__format">' . __('Brochure format', 'lionlab') . '</h6>';
			echo '<div class="items"><p><em>' . $size . '</em></p></div>';
		}
	}


	//
	add_action( 'woocommerce_after_shop_loop_item', 'my_extra_button_on_product_page', 10, 0 );


	function my_extra_button_on_product_page() {
	  $pdf = get_field('pdf_file'); 
	  global $product;

	  //output button if has pdf link
	  if ($pdf) {
	   echo '<a class="button" target="_blank" href="' . $pdf . '">Download</a>';
	  }

	}


	//sort products by category on shop page
	add_filter( 'woocommerce_product_subcategories_args', 'ts_woocommerce_get_subcategories_ordering_args' );

	function ts_woocommerce_get_subcategories_ordering_args( $args ) {
		$args['order'] = 'desc';
		$args['orderby'] = 'title';
		return $args;
	}


	//show category description on woocommerce product category pages
	add_action( 'woocommerce_after_subcategory_title', 'custom_add_product_description', 12);

	function custom_add_product_description ($category) {

		$cat_id = $category->term_id;
		$prod_term = get_term($cat_id,'product_cat');
		$description = $prod_term->description;

		echo '<div itemprop="description"><p>'.$description.'</p></div>';
		echo '<span class="btn btn--back">' . __('Se brochurer', 'lionlab') . '</span>';
	}
 ?>