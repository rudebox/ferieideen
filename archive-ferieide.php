<?php get_template_part('parts/header'); ?>

<main>

<?php get_template_part('parts/page', 'header'); ?>

  <section class="archive padding--both">
  	<div class="wrap hpad">
  		<div class="row flex flex--wrap"> 

		  <?php if (have_posts()): ?>
		    <?php while (have_posts()): the_post(); ?>

		     
		     <?php 
	          //Get category id to match up with archive filter
	          $cats = get_the_category();
	          $cat_string = '';

	          foreach ($cats as $cat) {
	            
	          } 
        	?>

	    	<?php 	
	    		//get thumb
	    		$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'url' );
	    		//post img alt tag
              	$alt = get_post_meta($thumb, '_wp_attachment_image_alt', true); 
	    	?>

		    <div class="archive__item col-sm-4 is-animated fade-up" href="<?php the_permalink(); ?>" itemscope itemtype="http://schema.org/Product">

				<img class="b-lazy" itemprop="thumbnail" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="<?php echo esc_url($thumb[0]); ?>" alt="<?php echo $alt; ?>">

				<header>
					<h3 class="archive__title" itemprop="name" title="<?php the_title_attribute(); ?>">
					<?php the_title(); ?>
					</h3>
				</header>

				<div class="archive__excerpt">
					<?php the_excerpt(); ?>
				</div>
				
				<a href="<?php the_permalink(); ?>" class="btn archive__btn">Se produkt</a>
		    </div>

		    <?php endwhile; else: ?>

		      <p>No posts here.</p>

		  <?php endif; ?>
	  </div>
	</div>

  </section>

</main>

<?php get_template_part('parts/footer'); ?>