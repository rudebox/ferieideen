<?php if (is_front_page() || is_archive() ) : ?>
<form class="search-result__form search-result__form--large" method="get" autocomplete="off" action="<?php bloginfo('url'); ?>/">
<?php else: ?>
<form class="search-result__form" method="get" autocomplete="off" action="<?php bloginfo('url'); ?>/">
<?php endif; ?>
	<input class="search-result__input controls pac-target-input" id="pac-input" type="text" value="<?php the_search_query(); ?>" placeholder="<?php _e('Søg eller udforsk via kort', 'lionlab'); ?>" name="s" id="s"></input> 
	<?php do_action( 'wpml_add_language_form_field' ); ?>
	<button class="btn btn--search" type="submit" ><?php echo file_get_contents('wp-content/themes/ferieideen/assets/img/search.svg'); ?></button> 
</form>