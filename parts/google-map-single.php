<?php 
    // Prepare
    $attributes = '';

    // Zoom
    $zoom = get_field('maps_zoom', 'options') ?: 12;
    $attributes .= " data-zoom='$zoom'";

    // Maps settings
    if ( $settings = get_field('maps_settings', 'options') ) {
        foreach ( $settings as $setting ) {
            $attributes .= " data-$setting='true'";
        }
    }

    //marker

    //color
    if ( $marker_color = get_field('marker_color', 'options') ) {
        $attributes .= "data-color='$marker_color'";
    }

    //fillopacity, used for inner color too
    $marker_fillopacity = get_field('marker_fillopacity', 'options');
        if ( $marker_fillopacity ) {
        $attributes .= "data-fillopacity='$marker_fillopacity'";
    }

    //inner color
    $marker_color_inner = get_field('marker_color_inner', 'options');
        if ( $marker_color_inner && $marker_fillopacity == true ) {
        $attributes .= "data-color-inner='$marker_color_inner'";
    }

    //strokeweight
    if ( $marker_strokeweight = get_field('marker_strokeweight', 'options') ) {
        $attributes .= "data-strokeweight='$marker_strokeweight'";
    }

    //marker size
    if ( $marker_size = get_field('marker_size', 'options') ) {
        $attributes .= "data-size='$marker_size'";
    }

    //custom marker
    $marker = get_field('maps_marker', 'options');
        if ( $marker && get_field('add_custom_marker', 'options') ) {
        $attributes .= ' data-marker="' . $marker['sizes']['thumbnail'] . '"';
    }

    //snazzymap
    if ( $snazzy = get_field('add_snazzy', 'options') ) {
        $snazzymap = get_field('snazzymap', 'options');
        $attributes .= " data-styles='$snazzymap'";
    }
 ?>

    <div class="google-map google-map--no-controls js-maps" <?php echo $attributes; ?>>

        <?php
            //google map    
            $location = get_field('location');
            $location['address'];
            $address = get_field('address') ?: $location['address'];
            $url = get_field('website');

            //post img
            $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'url' );

            //post img alt tag
            $alt = get_post_meta($thumb, '_wp_attachment_image_alt', true);  
         ?>
            
            <div class="marker" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>" data-icon="<?php echo $icon['url']; ?>">
                <div class="google-map__thumb google-map__thumb--small" style="background-image: url(<?php echo esc_url($thumb[0]); ?>);"></div>
                <h6 class="google-map__title"><?php the_title(); ?></h6>
                <a class="google-map__link" target="_blank" href="<?php echo esc_url($url); ?>"><?php echo esc_html($url); ?></a><br><br>
                <p><?php echo $address; ?></p>
            </div>

    </div>