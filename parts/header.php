<!doctype html>

<html <?php language_attributes(); ?> class="has-js">

<head>
  <meta charset="utf-8">
  <title><?php wp_title( '|', true, 'right' ); ?></title>
  <?php wp_head(); ?>
</head>

<body <?php body_class(); ?> itemscope itemtype="https://schema.org/WebPage">

<header class="header" id="header" itemscope itemtype="http://schema.org/WPHeader">
  <div class="wrap hpad flex flex--justify header__container">

    <?php 
      //logo SEO markup
      $site_name = get_bloginfo( 'name' );
      $logo_markup  = ( is_front_page() ) ? '<h1 class="visuallyhidden">' . $site_name . '</h1>' : '<p class="visuallyhidden">' . $site_name . '</p>';
    ?>


    <a class="header__logo" href="<?php bloginfo('url'); ?>">
      <img src="<?php echo get_template_directory_uri(); ?>/assets/img/logo_scaled.png" alt="<?php bloginfo('name'); ?>">
      <?php echo $logo_markup; ?>
    </a>

    <div class="nav-toggle"> 
      <span class="nav-toggle__label"><?php _e('Menu', 'lionlab') ?></span> 
      <span class="nav-toggle__icon"></span>
    </div>

    <nav class="nav" role="navigation" itemscope itemtype="http://schema.org/SiteNavigationElement">
      <div class="nav--mobile">
        <?php scratch_main_nav(); ?>

        <li class="nav__item nav__item--cart">
          <a class="nav__link nav__link--cart" href="<?php echo wc_get_cart_url(); ?>" title="<?php _e( 'Se din kurv' ); ?>">
            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/cart.png" alt="cart">
            <span><?php echo WC()->cart->get_cart_contents_count(); ?></span>
          </a>
        </li>
      </div>
    </nav>

  </div>
</header>
