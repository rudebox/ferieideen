<?php
	//get current top level category and echo its children - if any
	$current_cat_id = get_queried_object_id();
	$terms = get_terms([
	    'taxonomy' => get_queried_object()->taxonomy,
	    'parent'   => $current_cat_id,
	]);
?>


<div class="archive__submenu flex flex--wrap">
	<?php if ($terms) : ?>
	    <?php foreach($terms as $term) : ?>          
	      <a href="<?php echo get_term_link($term); ?>" class="archive__filter btn--filter"><?php echo $term->name; ?><span>/</span></a> 
	    <?php endforeach; ?>

	    <?php else: ?>

		<a class="archive__btn btn--back" onclick="window.history.go(-1); return false;"><?php _e('Tilbage', 'lionlab'); ?></a>

    <?php endif; ?>
</div>
