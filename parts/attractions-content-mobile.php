<?php 
  //post img
  $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'url' );
  //post img alt tag
  $alt = get_post_meta($thumb, '_wp_attachment_image_alt', true);   

  //ACF variables
  $adress = get_field('address');
  $mail = get_field('email');
  $url = get_field('website');
  $location = get_field('location');

  // Remove http:// or https:// from website field
  $url = preg_replace( "#^[^:/.]*[:/]+#i", "", $url );
?>

<div class="attractions__item--full is-hidden hidden-desktop col-sm-12" id="attractions--<?php the_title_attribute(); ?>">
  <div class="row flex flex--wrap">

    <a href="<?php echo the_permalink(); ?>" class="attractions__img col-sm-4" style="background-image: url(<?php echo esc_url($thumb[0]); ?>);">
      
    </a>

    <div class="attractions__excerpt col-sm-4" itemprop="description">
      <header>
        <h2 class="attractions__title h3" itemprop="name">                                
            <?php the_title(); ?>
        </h2>
      </header>

      <?php the_excerpt(); ?>

      <a href="<?php echo the_permalink(); ?>" class="btn btn--readmore"><?php _e('Læs mere', 'lionlab'); ?> <span>+</span></a>
    </div>
    
    <div class="attractions__info col-sm-4">
      <?php if ($adress) : ?>
      <h6 class="attractions__info-title"><?php _e('Adresse', 'lionlab'); ?></h6>
      <p itemprop="address"><?php echo $adress; ?></p>
      <?php endif; ?>

      <?php if ($mail) : ?>
      <h6 class="attractions__info-title"><?php _e('Kontakt', 'lionlab'); ?></h6>
      <a href="mailto:<?php echo esc_html($mail); ?>"><?php echo esc_html($mail); ?></a>
      <?php endif; ?>

      <?php if ($url) : ?>
      <h6 class="attractions__info-title"><?php _e('Website', 'lionlab'); ?></h6>
      <a target="_blank" href="<?php echo esc_url($url); ?>"><?php echo esc_url($url); ?></a>
      <?php endif; ?>

      <?php if ($location) : ?>
      <h6 class="attractions__info-title"><?php _e('Map', 'lionlab'); ?></h6>
      <?php get_template_part('parts/google-map', 'single');  ?>
      <?php endif; ?>
    </div>

  </div>
</div>