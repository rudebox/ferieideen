<?php
    /**
    * Google Maps
    * @since 2.4.0
    **/

    // Prepare
    $attributes = '';

    // Zoom
    $zoom = get_field('maps_zoom', 'options') ?: 12;
    $attributes .= " data-zoom='$zoom'";

    // Maps settings
    if ( $settings = get_field('maps_settings', 'options') ) {
        foreach ( $settings as $setting ) {
            $attributes .= " data-$setting='true'";
        }
    }

    /*
    * Marker
    */

    // Color
    if ( $marker_color = get_field('marker_color', 'options') ) {
        $attributes .= "data-color='$marker_color'";
    }

    // Fillopacity, used for inner color too
    $marker_fillopacity = get_field('marker_fillopacity', 'options');
        if ( $marker_fillopacity ) {
        $attributes .= "data-fillopacity='$marker_fillopacity'";
    }

    // Inner color
    $marker_color_inner = get_field('marker_color_inner', 'options');
        if ( $marker_color_inner && $marker_fillopacity == true ) {
        $attributes .= "data-color-inner='$marker_color_inner'";
    }

    // Strokeweight
    if ( $marker_strokeweight = get_field('marker_strokeweight', 'options') ) {
     $attributes .= "data-strokeweight='$marker_strokeweight'";
    }

    // Marker size
    if ( $marker_size = get_field('marker_size', 'options') ) {
        $attributes .= "data-size='$marker_size'";
    }

    // Custom marker
    $marker = get_field('maps_marker', 'options');
        if ( $marker && get_field('add_custom_marker', 'options') ) {
        $attributes .= ' data-marker="' . $marker['sizes']['thumbnail'] . '"';
    }

    // Snazzymap
    if ( $snazzy = get_field('add_snazzy', 'options') ) {
        $snazzymap = get_field('snazzymap', 'options');
        $attributes .= " data-styles='$snazzymap'";
    }

    //query arguments
    $args = array(
      'posts_per_page' => -1,
      'post_type' => 'ferieide'
    );
     
    $query = new WP_QUERY($args);

    if ( $query->have_posts() ) : 

    ?>

    <section class="search-result">
        <div class="wrap hpad">

            <?php 
                //get top level categories for filtering thorugh query
                $categories = get_categories( array(
                    'orderby' => 'name',
                    'parent'  => 0
                ) );
            ?>

            <div class="row">

                <div class="col-sm-4 col-md-2">
                
                <form class="search-result__form" method="get" autocomplete="off" action="<?php bloginfo('url'); ?>/">
                      <input id="pac-input" class="controls search-result__input search-result__input--small" type="text" placeholder="<?php _e('By, adresse, oplevelse', 'lionlab'); ?>" value="<?php the_search_query(); ?>" name="s" id="s">
                      <?php do_action( 'wpml_add_language_form_field' ); ?>
                      <?php echo file_get_contents('wp-content/themes/ferieideen/assets/img/map.svg'); ?>
                </form>
            
                  <form class="search-result__filter">
                      
                      <p><?php echo file_get_contents('wp-content/themes/ferieideen/assets/img/search.svg'); ?> <?php _e('Kategorier', 'lionlab'); ?></p>  

                      <label for="Alle">
                        <input class="search-result__checkbox search-result__checkbox--all" type="radio" checked="checked" id="all" name="filter" value="all" onchange="filterMarker(this.value);"></input>
                        <?php _e('Alle', 'lionlab'); ?>
                      </label>

                      <?php foreach ($categories as $category) : ?>
                        <label for="<?php echo $category->name; ?>">
                            <input class="search-result__checkbox" type="radio" id="<?php echo $category->name; ?>" name="filter" value="<?php echo $category->name; ?>" onchange="filterMarker(this.value);"></input>
                            <?php echo $category->name; ?>
                        </label>
                      <?php endforeach; ?>
                  </form>

                </div>

                <div class="google-map google-map--large js-maps col-sm-8 col-md-10" id="googlemaps" <?php echo $attributes; ?>>

                    <?php
                    // Loop Google Maps
                    while ( $query->have_posts() ) : $query->the_post();
                        $location = get_field('location');
                        $icon = get_field('marker_img');
                        $address = get_field('address') ?: $location['address'];
                        $url = get_field('website');

                        //post img
                        $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'url' );

                        //post img alt tag
                        $alt = get_post_meta($thumb, '_wp_attachment_image_alt', true);  

                         //get primary category - see function in lionlab-helpers.php
                        $post_categories = get_post_primary_category($post->ID, 'category');

                        $primary_category = $post_categories['primary_category']->name;
                     ?>
        
                    <?php if ($location) : ?>
                        <div class="marker google-map__marker" data-category="<?php echo $primary_category; ?>"  data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>" data-icon="<?php echo $icon['url']; ?>">
                            
                            <?php if ($thumb) : ?>
                            <a href="<?php the_permalink(); ?>" class="google-map__thumb" style="background-image: url(<?php echo esc_url($thumb[0]); ?>);"></a>
                            <?php endif; ?>
                            
                            <a href="<?php the_permalink(); ?>">
                                <h6 class="google-map__title"><?php the_title(); ?></h6>
                            </a>

                            <?php if ($url) : ?>
                            <a class="google-map__link" target="_blank" href="<?php echo esc_url($url); ?>"><?php echo esc_html($url); ?></a><br><br>
                            <?php endif; ?>
                            
                            <?php if ($address) : ?>
                            <p><?php echo $address; ?></p>
                            <?php endif; ?>

                        </div>
                    <?php endif; ?>

                    <?php endwhile; wp_reset_postdata();  ?>

                </div>
            </div>
        </div>
    </section>    
<?php endif; ?>