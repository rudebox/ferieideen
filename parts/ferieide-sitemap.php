 <?php

/*
 * Template Name: Ferieide sitemap
 */

get_template_part('parts/header'); the_post();

//query arguments
$args = array(
  'posts_per_page' => -1,
  'post_type' => 'ferieide',
  'orderby' => 'ASC'
);

//the query
$query = new WP_Query( $args );

if ($query->have_posts() ) : 
?>

<section id="sitemap" class="sitemap padding--both">
	<div class="wrap hpad">

		<form>
			<input class="btn btn--back" type="button" value="Print denne side" onClick="window.print()">
		</form>

		<div class="row">

			<?php 
	          while ($query->have_posts() ): $query->the_post(); 
	        ?>

	        	<div class="col-sm-12 sitemap__item">
	        		<h4 class="sitemap__title"><?php the_title(); ?></h4>
	        		Oprettet: <?php echo get_the_date(); ?><br>
	        		<?php echo the_permalink(); ?>
	        	</div>

	        <?php endwhile; wp_reset_postdata(); ?>

         </div>
	</div>
</section>
<?php endif; ?>

<?php get_template_part('parts/footer'); ?>