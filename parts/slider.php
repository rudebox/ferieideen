<?php
/**
 * Description: Lionlab global slider
 *
 * @package Lionlab
 * @subpackage Lionlab
 * @since Version 1.0
 * @author Kaspar Rudbech
*/

if (have_rows('slides', 'options') ) : ?>
  
  <section class="slider">
    <div class="wrap hpad">
      <div class="slider__track is-slider">

        <?php
        // Loop through slides
        while (have_rows('slides', 'options') ) :
          the_row();
          $image   = get_sub_field('slides_bg');
          $title = get_sub_field('slides_title');
          $caption = get_sub_field('slides_text'); ?>

          <div class="slider__item flex flex--valign" style="background-image: url(<?php echo esc_url($image['url']); ?>);">
            <div class="wrap hpad slider__container">
              <div class="slider__text">
                <h2 class="slider__title h1"><?php echo $title; ?></h2>
                <?php echo $caption; ?>
              </div>
            </div>
          </div>

        <?php endwhile; ?>

      </div>
    </div>
  </section>
<?php endif; ?>