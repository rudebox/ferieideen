<?php 
/**
* Description: Retrieve latests post from category attraction 
*
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/

//section settings 
$section_title = get_sub_field('header');
$text = get_sub_field('text');
$margin = get_sub_field('margin');
$category_id = get_sub_field('cat');

//query arguments
$args = array(
  'posts_per_page' => 7,
  'orderby' => 'rand',
  'post_type' => 'ferieide',
  'cat' => $category_id
);

//retrieve category link from the category id
$term_link = get_term_link($category_id);

//the query
$query = new WP_Query( $args );

if ( $query->have_posts() ) :

//counter
$i = 0;
?>

<section class="attractions padding--<?php echo esc_attr($margin); ?>">
  <div class="wrap hpad">
    <div class="flex flex--wrap">

      <?php if ($section_title) : ?>
        <a href="<?php echo $term_link; ?>" class="attractions__intro col-sm-3">
          <h2 class="attractions__header"><?php echo esc_html($section_title); ?></h2>
          <p><?php echo esc_html($text); ?></p>
          <span class="btn btn--large">+</span>
        </a>
      <?php endif; ?>


      <?php 
        //the Loop
        while ( $query->have_posts() ) : $query->the_post();

        //post img
        $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'url' );
        //post img alt tag
        $alt = get_post_meta($thumb, '_wp_attachment_image_alt', true);   

        //ACF variables
        $adress = get_field('address');
        $mail = get_field('email');
        $url = get_field('website'); 

        // Remove http:// or https:// from website field
        $url = preg_replace( "#^[^:/.]*[:/]+#i", "", $url );

        $i++;
      ?>

        <button title="<?php the_title_attribute(); ?>" itemscope itemtype="http://schema.org/TouristAttraction" class="col-sm-3 attractions__item" id="attractions--<?php the_title_attribute(); ?>" data-target="attractions--<?php the_title_attribute(); ?>" style="background-image: url(<?php echo esc_url($thumb[0]); ?>);">
            
            <div class="attractions__content" id="attractions--<?php the_title_attribute(); ?>" data-target="attractions--<?php the_title_attribute(); ?>">
              <header>
                <h2 class="attractions__title h4" itemprop="name">                                
                    <?php the_title(); ?>
                </h2>
              </header>

              <div class="attractions__excerpt" itemprop="description">
                <?php echo wp_trim_words(get_the_excerpt(), 20, '...' ); ?>
              </div>
              
              <div class="attractions__wrap flex flex--justify flex--center flex--wrap">
                <a class="attractions__permalink" href="<?php echo the_permalink(); ?>"></a> 
                <a class="attractions__sitelink" target="_blank" href="<?php echo esc_url($url); ?>"><?php echo esc_html($url); ?></a> <span class="btn btn--large">+</span>
              </div>
            </div>
            
            <header class="bg--sand attractions__label">
              <h2 class="attractions__title h4" itemprop="name">                                
                <?php the_title(); ?>
              </h2>
          </header>
        </button>

      <?php 
        endwhile;
        //restore original post data
        wp_reset_postdata();
      ?>
    

    </div>
  </div>
</section>
<?php endif; ?>