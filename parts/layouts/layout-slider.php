<?php
/**
 * Description: Lionlab sliders
 *
 * @package Lionlab
 * @subpackage Lionlab
 * @since Version 1.0
 * @author Kaspar Rudbech
*/

if ( have_rows('slides') ) : ?>

  <section class="slider">
    <div class="wrap hpad">
      <div class="slider__track--front is-slider">

        <?php
        // Loop through slides
        while ( have_rows('slides') ) :
          the_row();
          $image   = get_sub_field('slides_bg');
          $title = get_sub_field('slides_title');
          $caption = get_sub_field('slides_text'); ?>

          <div class="slider__item flex flex--valign" style="background-image: url(<?php echo esc_url($image['url']); ?>);">
            <div class="wrap hpad slider__container flex flex--hvalign">
              <div class="slider__text">
                <h2 class="slider__title h1"><?php echo $title; ?></h2>
                <?php echo $caption; ?>
              </div>
            </div>
          </div>

        <?php endwhile; ?>

      </div>
      
      <div class="slider__search">
      <?php 
          if (!is_single() ) {
            get_template_part('parts/search-bar'); 
          }
        ?> 
      </div>

    </div>
  </section>
<?php endif; ?>