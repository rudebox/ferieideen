<?php 
/**
* Description: Lionlab gallery field group layout
*
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/

//gallery
$gallery = get_field('gallery'); 
$index = get_row_index();

$i=0;


if ( $gallery ) : ?> 

<div class="gallery col-lg-4 single__info-column" itemscope itemtype="ImageGallery">

	<h6 class="gallery__title single__info-title"><?php _e('Galleri', 'lionlab'); ?></h6>

	<div class="gallery__list flex flex--wrap">

		<?php
			// Loop through gallery
			foreach ( $gallery as $image ) : 

			$i++;

			//show only 4 visible thumbnails
			if ($i >= 5) :
				$class = "is-hidden";

			endif;
			
		?>


			<div class="gallery__item <?php echo esc_attr($class); ?>" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
				<a href="<?= $image['sizes']['large']; ?>" class="js-zoom gallery__link" data-fancybox="gallery-<?= $index; ?>" itemprop="contentUrl" data-caption="<?= $image['caption']; ?>" title="<?= $image['title'] ?>" data-width="<?= $image['sizes']['large-width']; ?>" data-height="<?= $image['sizes']['large-height']; ?>">
			
				<img class="gallery__image" data-src="<?= $image['sizes']['gallery']; ?>" src="<?= $image['sizes']['gallery']; ?>" alt="<?= $image['alt']; ?>" itemprop="thumbnail" height="<?= $image['sizes']['large-height']; ?>" width="<?= $image['sizes']['large-width']; ?>">
				</a>
			</div>
		<?php endforeach; ?>

	</div>

</div>

<?php endif; ?>