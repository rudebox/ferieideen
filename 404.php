<?php get_template_part('parts/header'); ?>

<main>

  <?php get_template_part('parts/slider'); ?>

  <?php get_template_part('parts/page', 'header'); ?>

  <section class="error">
  	<div class="wrap hpad">
   	 <p><?php _e('Beklager, men vi kunne ikke finde siden du søgte efter', 'lionlab'); ?></p>
    </div>
  </section>

</main>

<?php get_template_part('parts/footer'); ?>