<?php get_template_part('parts/header'); ?>

<main itemscope itemtype="http://schema.org/SearchResultsPage">

  <?php 
    //include template parts
    get_template_part('parts/slider'); 
    get_template_part('parts/page', 'header'); 
    get_template_part('parts/google', 'maps'); 

    //count serch results
    $allsearch = new WP_Query("s=$s&showposts=0"); 

   ?>

    <section class="search-result">
      <div class="wrap hpad">
        <div class="flex flex--wrap">
          <?php if (have_posts() ) : while (have_posts()): the_post(); ?>

            <?php 
              //post img
              $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'url' );
              //post img alt tag
              $alt = get_post_meta($thumb, '_wp_attachment_image_alt', true);   

              //get primary category - see function in lionlab-helpers.php
              $post_categories = get_post_primary_category($post->ID, 'category');

              $primary_category = $post_categories['primary_category']->name;

              $url = get_field('website');
              // Remove http:// or https:// from website field
              $url = preg_replace( "#^[^:/.]*[:/]+#i", "", $url );
            ?>

            <button data-category="<?php echo $primary_category; ?>" title="<?php the_title_attribute(); ?>" id="attractions--<?php the_title_attribute(); ?>" data-target="attractions--<?php the_title_attribute(); ?>" class="col-sm-3 attractions__item attractions__item--shuffle search-result__item" style="background-image: url(<?php echo esc_url($thumb[0]); ?>);">

                <div class="attractions__content" id="attractions--<?php the_title_attribute(); ?>" data-target="attractions--<?php the_title_attribute(); ?>">
                  <header>
                  <h2 class="attractions__title h4">                                
                    <?php the_title(); ?>
                  </h2>
                  </header>

                  <div class="attractions__excerpt">
                  <?php echo wp_trim_words(get_the_excerpt(), 20, '...' ); ?>
                  </div>

                  <div class="attractions__wrap flex flex--justify flex--center">
                    <a class="attractions__permalink" href="<?php echo the_permalink(); ?>"></a> 
                    <a class="attractions__sitelink" target="_blank" href="<?php echo esc_url($url); ?>"><?php echo esc_html($url); ?></a> <span class="btn btn--large">+</span>
                  </div>
                </div>

                <header class="bg--sand attractions__label">
                  <h2 class="attractions__title h4" itemprop="name">                                
                    <?php the_title(); ?>
                  </h2>
                </header>
            </button>
            <?php endwhile; else: ?>
              
            <div class="search-result__query padding--top">
              <h4><?php _e('Din søgning for', 'lionlab'); ?> <strong><?php echo esc_attr(get_search_query()); ?></strong> <?php _e('gav ingen resultater', 'lionlab'); ?></h4>
            </div>
            <?php endif; ?>
        </div>
      </div>
    </section>

</main>

<?php get_template_part('parts/footer'); ?>