<?php get_template_part('parts/header'); the_post(); ?>

<main>

  <?php 
    get_template_part('parts/slider');
    get_template_part('parts/page', 'header');
  ?>

  <section class="page">

    <div class="wrap hpad">

      <article class="checkout">

        <?php the_content(); ?>

      </article>

    </div>

  </section>

</main>

<?php get_template_part('parts/footer'); ?>